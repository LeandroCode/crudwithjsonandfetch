function fetchConsult(comando) {

    url = "http://localhost:8080/CRUDWITHFETCH/Buscarxid?&";

    elobjeto = {};

    elobjeto.filtroId = document.querySelector('#texto').value;

    fetch(url + "filtroId=" + elobjeto.filtroId)
            .then(
                    response => response.json()
            )
            .then(comando => {

                var tablaAutos = "";

                for (var i = 0; i < comando.length; i++) {
                    tablaAutos += "<tr><td>" + comando[i].auto_id + "</td><td>"
                            + "  " + comando[i].auto_marca + "</td><td>"
                            + "  " + comando[i].auto_modelo + "</td><td>"
                            + "  " + comando[i].auto_precio + "</td><td>"
                            + "  " + comando[i].auto_color + "</td></tr>" + "<br>";
                }

                document.getElementById("traerResultado").innerHTML = tablaAutos;
            })
            .catch(error => console.log('ERROR AL SOLICITAR DATOS'));


}

document.getElementById("elboton").addEventListener("click", function () {
    fetchConsult();
});

function fetchInsert() {

    url = "http://localhost:8080/CRUDWITHFETCH/Buscarxid?&";

    elobjeto = {};

    elobjeto.marca = document.querySelector('#marca').value;
    elobjeto.modelo = document.querySelector('#modelo').value;
    elobjeto.precio = document.querySelector('#precio').value;
    elobjeto.color = document.querySelector('#color').value;

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(elobjeto),
        headers: {
            "Content-type": "application/json"
        }
    })
    .then(data => {

        fetchConsult(data);

    });
}

document.getElementById("botonInsert").addEventListener("click", function () {
    fetchInsert();
});

function fetchUpdate() {

    url = "http://localhost:8080/CRUDWITHFETCH/Buscarxid?&";

    elobjeto = {};

    elobjeto.getIDx = document.querySelector('#idAuto').value;
    elobjeto.marcax = document.querySelector('#marca1').value;
    elobjeto.modelox = document.querySelector('#modelo1').value;
    elobjeto.preciox = document.querySelector('#precio1').value;
    elobjeto.colorx = document.querySelector('#color1').value;

    fetch(url, {
        method: 'PUT',
        body: JSON.stringify(elobjeto),
        headers: {
            "Content-type": "application/json"
        }
    })
    .then(data => {

         fetchConsult(data);

    });

}

document.getElementById("botonUpdate").addEventListener("click", function () {
    fetchUpdate();
});

function fetchDelete() {

    url="http://localhost:8080/CRUDWITHFETCH/Buscarxid?&";

    elobjeto = {};

    elobjeto.idAutox = document.querySelector('#idDelete').value;

    fetch(url+ "q=" + elobjeto.idAutox, {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
    })
    .then(data => {
            fetchConsult(data);
    });

}

document.getElementById("botonDELETE").addEventListener("click", function () {
    fetchDelete();
});