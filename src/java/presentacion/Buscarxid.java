package presentacion;

import entidad.Dao;
import com.google.gson.Gson;
import entidad.AdministraciondeBasedeDatos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Buscarxid extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        Gson convertir = new Gson();

        String filtro = request.getParameter("filtroId");

        if (filtro != null && filtro.length() > 0) {
            //The specified key is stored in a variable.
            String elvalor = filtro;
            //We look for the specific row through the value entered by the user and store it in a variable. 
            ArrayList<TreeMap> valorFinal = Dao.BuscarXindice(Integer.parseInt(elvalor));
            //This condition evaluates if the variable contains information.
            if (valorFinal != null) {
                //It converts the value to json format and displays it to the navigation website.
                out.print(convertir.toJson(valorFinal));
            }

        } else {
            //This variable stores all the rows in the table.
            ArrayList<TreeMap> listado = Dao.Listado();
            //It evaluates if the variable is not null. 
            if (listado != null) {
                //The whole table is converted to JSON and stored in a variable.
                String resultado = convertir.toJson(listado);
                //We send the result to the client
                out.println("" + resultado);
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //It sets the response format in text
        response.setContentType("text/html;charset=UTF-8");
        //This variable stores the reading of the data entered by the user.
        BufferedReader reader = request.getReader();
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It returns a treemap with the content of the variable whose name is 'reader'.
        TreeMap<String, String> param_auto = convertir.fromJson(reader, TreeMap.class);
        //It stores the values obtained from the database in a string.
        String marca = param_auto.get("marca");
        String modelo = param_auto.get("modelo");
        String precio = param_auto.get("precio");
        String color = param_auto.get("color");

        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministraciondeBasedeDatos.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("INSERT into autos (au_id,au_marca,au_modelo,au_precio,au_color)"
                    + " VALUES (null,?,?,?,?)");
            //It sets each variable submitted by the user from the website to the database. 
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, precio);
            sentencia.setString(4, color);

            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            sentencia.close();
            conexion.close();
            //Connection error
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        //This variable stores the reading of the data entered by the user.
        BufferedReader reader = req.getReader();
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It returns a treemap with the content of the variable whose name is 'reader'.
        TreeMap<String, String> param_auto = convertir.fromJson(reader, TreeMap.class);
        //It stores the values obtained from the database in a string.
        String idAuto = param_auto.get("getIDx");
        String marca = param_auto.get("marcax");
        String modelo = param_auto.get("modelox");
        String precio = param_auto.get("preciox");
        String color = param_auto.get("colorx");

        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministraciondeBasedeDatos.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("UPDATE autos SET au_marca = ?,au_modelo = ?,au_precio = ?,au_color = ?"
                    + "WHERE au_id = ?;");
            //It sets each variable submitted by the user from the website to the database.
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, precio);
            sentencia.setString(4, color);
            sentencia.setInt(5, Integer.parseInt(idAuto));
            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            sentencia.close();
            conexion.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        //It constructs a gson object.
        Gson convertir = new Gson();
        //It stores a string with the content of the parameter.
        String filtroErased = req.getParameter("q");
        //The specified key is stored in a string.
        String idElim = filtroErased;

        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection conexion = AdministraciondeBasedeDatos.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = conexion.prepareStatement("DELETE FROM autos WHERE au_id = ?");
            //the index to be deleted is setted in the database.
            sentencia.setInt(1, Integer.parseInt(idElim));
            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            System.out.println("!!!Borrado exitoso!!!");
            sentencia.close();
            conexion.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }
}
